# Ziggy Buildbox

A docker image to make cross-compiling statically linked nim executables quick and easy, with inspiration from [zig](https://andrewkelley.me/post/zig-cc-powerful-drop-in-replacement-gcc-clang.html) and the [nim forums](https://forum.nim-lang.org/t/6362), since I utterly failed in my attempts with [holy build box](https://github.com/phusion/holy-build-box).

The docker image is built on `nim-lang:alpine` with `zig` installed (along with a script to effectively make `zcc` an alias for `zig cc`). This allows you to use `zig` as a c compiler that knows how to build for other architectures and operating systems.

## How to use ziggy-buildbox

### As a base image for your build image

You use the image as a base image for building your own `Dockerfile` (possibly as part of a [multi-stage build](https://docs.docker.com/develop/develop-images/multistage-build/)).

### As a commandline environment

You can enter docker image by running:

```sh
> # Fish shell
> docker run --rm -it -v (pwd):/usr/src/app --entrypoint /bin/sh maxgrenderjones/ziggy-buildbox

$ # Bash shell
$ docker run --rm -it -v "$(pwd)":/usr/src/app --entrypoint /bin/sh maxgrenderjones/ziggy-buildbox
```

Note that once zig is installed, you need to tell nim to use it as the compiler. Some examples of how to do this:

```sh
> # To build for arm64 (Raspberry Pi 4 on a 64 bit OS)
> nim c --cpu:arm64 --cc:clang --clang.exe="zcc" --clang.linkerexe="zcc" --passC:"-target aarch64-linux-musl" --passL:"-target aarch64-linux-musl" -o:hello.aarch64 hello.nim
> # To build for armhf (Raspberry Pi 1-4 on a 32 bit OS)
> nim c --cpu:arm --cc:clang --clang.exe="zcc" --clang.linkerexe="zcc" --passC:"-target arm-linux-musleabihf" --passL:"-target arm-linux-musleabihf" -o:hello.armhf hello.nim 
> # To build for amd64
> nim c --cc:clang --clang.exe="zcc" --clang.linkerexe="zcc" --passC:"-target x86_64-linux-musl" --passL:"-target x86_64-linux-musl" -o:hello.amd64 hello.nim
```

Ziggy Buildbox includes a command `zimc` that runs `nim c` for you and automatically adds the necessary switches for cross-compilation.

```sh
> zimc --help
Usage:
  zimc <src>
  zimc -h|--help

Arguments:
  <src>                    File to build

Options:
  --target=<target>        Which target to build for
  --outdir=<outdir>        Where to write output files to
  -o, --out=<out>          Output filename
  -s, --strip              Strip the output (requires -o, --out)
  --pack                   Use upx to pack the binary (requires -o, --out)
  -v, --verbose            Verbose output
  -i, --install=<install>  Package(s) to install with nimble before building
  -h, --help               Show help message
```

Supported targets:

- target -> arch / os / libc
- `amd64` -> `amd64` / `linux` / `musl`
- `armhf` -> `arm` / `linux` / `muscleabihf`
- `arm64` -> `aarch64` / `linux` / `musl`
- `mipsel` -> `mipsel` / `linux` / `musl`
- `osx` -> `amd64` / `macosx` / `gnu`

e.g. To build an statically-linked armhf build of `zimc` (for use on a raspberry pi, for example) from inside the image, you can run:

```sh
> nimble install therapist
> zimc --target arm64 -o zimc.armhf zimc.nim
```

### Running the image directly

`zimc` is set to be the entrypoint of the container, so from the outside, an equivalent command would be:

```sh
docker run --rm -it -v (pwd):/usr/src/app maxgrenderjones/ziggy-buildbox --target armhf -o zimc.armhf zimc.nim -v -i therapist
```

(See that `zimc` can invoke nimble for you and install packages inside your container).

## Limitations

There are some limitations that require improvements in zig to resolve:

- `zig` doesn't support cross-compiling for BSDs. Relevant issues:
    - [Tier 1 FreeBSD Support for x86_64](https://github.com/ziglang/zig/issues/1759)

- `zig cc` doesn't support having `cache-dir` not being in the current directory
    - [Zig issue 5061](https://github.com/ziglang/zig/issues/5061)

Zig provides a version of `libc` for multiple architectures / operating systems. For pure nim libraries, this should be enough. However, if you want to link to another C library (e.g. `openssl`) you're on your own - I'd assume you need to find a version that has already been built for your target.

## Notes

To cut a new release:

- Build the image: `docker build --pull --rm -f "Dockerfile" -t maxgrenderjones/ziggy-buildbox "."`
- Log in to docker: `docker login`
- Push the image: `docker push maxgrenderjones/ziggy-buildbox`
