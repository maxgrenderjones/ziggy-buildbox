import os
import sequtils
import strformat
import sugar
import tables

import therapist

# See https://andrewkelley.me/post/zig-cc-powerful-drop-in-replacement-gcc-clang.html for inspiration

let basic = @["--verbosity:1", "--stdout:on", "--colors:on"]

let compiler = @["--cc:clang", "--clang.exe=zcc", "--clang.linkerexe=zcc"]


# The following are flags to be passed to the nim compiler
# Use zig targets | jq .libc to get a list of targets
# See https://github.com/ziglang/zig/wiki/FAQ#why-do-i-get-illegal-instruction-when-using-with-zig-cc-to-build-c-code
# for why O3 is required
let targets = {
    "amd64": { # Without O2 get SIGILL
        "--cpu": "amd64",
        "--passC": "-O3 -target x86_64-linux-musl",
        "--passL": "-O3 -target x86_64-linux-musl",
        "--opt": "size",
    }.toTable(),
    "arm64": {
        "--cpu": "arm64",
        "--passC": "-O3 -target aarch64-linux-musl",
        "--passL": "-O3 -target aarch64-linux-musl",
        "--opt": "size",
    }.toTable(),
    "armhf": {
        "--cpu": "arm",
        "--passC": "-O3 -target arm-linux-musleabihf",
        "--passL": "-O3 -target arm-linux-musleabihf",
        "--opt": "size",
    }.toTable(),
    "mipsel": {
        "--cpu": "mipsel",
        "--passC": "-target mipsel-linux-musl",
        "--passL": "-target mipsel-linux-musl",
        "--opt": "size",
    }.toTable(),
    "osx": {
        "--cpu": "amd64",
        "--os": "macosx",
        "--passC": "-target x86_64-macos-gnu",
        "--passL": "-target x86_64-macos-gnu",
        "--opt": "size",
    }.toTable(),
    "win": {
        "--cpu": "amd64",
        "--os": "windows",
        "--passC": "-target x86_64-windows-gnu",
        "--passL": "-target x86_64-windows-gnu",
        "--opt": "size",
    }.toTable()
}.toTable()


proc run(command: string, verbose: bool) =
    if verbose:
        echo command
    let status = execShellCmd command
    if status != 0:
        quit(status)


if isMainModule:
    let spec = (
        src: newPathArg(@["<src>"], help="File to build"),
        target: newStringArg(@["--target"], required=true, choices = toSeq(targets.keys()), help="Which target to build for", helpvar="arch"),
        outdir: newDirArg(@["--outdir"], help="Where to write output files to", helpvar="dir"),
        outfile: newStringArg(@["-o", "--out"], help="Output filename", helpvar="file"),
        strip: newCountArg(@["-s", "--strip"], help="Strip the output (requires -o, --out)"),
        pack: newCountArg(@["--pack"], "Use upx to pack the binary (requires -o, --out)"),
        verbose: newCountArg(@["-v", "--verbose"], help="Verbose output"),
        install: newStringArg(@["-i", "--install"], help="Package(s) to install with nimble before building", multi=true, helpvar="pkg"),
        help: newHelpArg()
    )

    spec.parseOrQuit()

    let verbose = spec.verbose.seen

    if (spec.strip.seen or spec.pack.seen) and (not spec.outfile.seen):
        quit("--strip and/or --pack require --out", 1)

    var args = targets[spec.target.value]
    if spec.outdir.seen:
        args["--outdir"] = spec.outdir.value
    if spec.outfile.seen:
        args["--out"] = spec.outfile.value

    let argseq = collect(newSeq):
        for key, value in args.pairs:
            fmt"{key}:{value}"

    if spec.install.seen:
        let install = "nimble install -y " & quoteShellCommand(spec.install.values)
        install.run(verbose)

    let compile = "nim " & quoteShellCommand(@["c"] & basic & compiler & argseq & spec.src.values)
    compile.run(verbose)

    if spec.strip.seen:
        let strip = "llvm-strip " & quoteShellCommand(spec.outfile.values)
        strip.run(verbose)
    
    if spec.pack.seen:
        let pack = "upx --best " & quoteShellCommand(spec.outfile.values)
        pack.run(verbose)