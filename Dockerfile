FROM nimlang/nim:alpine

# Install zig
RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories && \
    apk add zig && \
    apk add llvm11-static && \
    apk add file upx tini


COPY zcc /usr/bin
# Prepare libc (this doesn't work yet as you can't tell `zig cc` where to find `zig-cache`)
# RUN echo 'echo "Hello world"' > hello.nim && \
#     nim c --cpu:arm64 --cc:clang --clang.exe="zcc" --clang.linkerexe="zcc" --passC:"-target aarch64-linux-musl" --passL:"-target aarch64-linux-musl" -o:hello.aarch64 hello.nim && \
#     nim c --cpu:arm --cc:clang --clang.exe="zcc" --clang.linkerexe="zcc" --passC:"-target arm-linux-musleabihf" --passL:"-target arm-linux-musleabihf" -o:hello.armhf hello.nim && \
#     nim c --cc:clang --clang.exe="zcc" --clang.linkerexe="zcc" --passC:"-target x86_64-linux-musl" --passL:"-target x86_64-linux-musl" -o:hello.amd64 hello.nim

COPY zimc.nim zimc.nim
RUN nimble install -y therapist && \
    nim c zimc && \
    mv zimc /usr/bin && \
    nimble -y remove therapist && \
    rm zimc.nim
    
WORKDIR /usr/src/app

ENTRYPOINT [ "/sbin/tini", "--", "/usr/bin/zimc" ]



